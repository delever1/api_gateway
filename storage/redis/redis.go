package redis

import (
	"context"
	"delever/api_gateway/config"
	"delever/api_gateway/storage"
	"fmt"

	"github.com/go-redis/cache/v9"
	goRedis "github.com/redis/go-redis/v9"
)

type cacheStrg struct {
	db    *cache.Cache
	cache *cacheRepo
}

func NewCache(ctx context.Context, cfg config.Config) (storage.RedisI, error) {
	redisClient := goRedis.NewClient(&goRedis.Options{
		Addr:     fmt.Sprintf("%s:%d", cfg.RedisHost, cfg.RedisPort),
		Password: cfg.RedisPassword,
	})
	redisCache := cache.New(&cache.Options{
		Redis: redisClient,
	})

	return &cacheStrg{
		db: redisCache,
	}, nil
}

func (s *cacheStrg) Cache() storage.CacheI {
	if s.cache == nil {
		s.cache = NewCacheRepo(s.db)
	}

	return s.cache
}
