package storage

import (
	"context"
	"time"
)

type RedisI interface {
	Cache() CacheI
}

type CacheI interface {
	Create(ctx context.Context, id string, obj interface{}, ttl time.Duration) error
	Get(ctx context.Context, id string, response interface{}) (bool, error)
	Delete(ctx context.Context, id string) error
}
