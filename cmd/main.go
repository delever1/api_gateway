package main

import (
	"context"
	"delever/api_gateway/api"
	"delever/api_gateway/api/handler"
	"delever/api_gateway/config"
	"delever/api_gateway/pkg/logger"
	"delever/api_gateway/services"
	"delever/api_gateway/storage/redis"
	"fmt"

	"github.com/gin-gonic/gin"
)

func main() {
	cfg := config.Load()

	fmt.Printf("config: %+v/n", cfg)

	// Setup Logger
	loggerLevel := logger.LevelDebug
	switch cfg.Environment {
	case config.DebugMode:
		loggerLevel = logger.LevelDebug
	case config.TestMode:
		loggerLevel = logger.LevelDebug
	default:
		loggerLevel = logger.LevelInfo
	}

	log := logger.NewLogger(cfg.ServiceName, loggerLevel)
	defer logger.Cleanup(log)

	grpcSrvc, err := services.NewGrpcClients(cfg)
	if err != nil {
		panic(err)
	}

	redisStrg, err := redis.NewCache(context.Background(), cfg)
	if err != nil {
		panic(err)
	}

	r := gin.New()

	h := handler.NewHandler(cfg, log, grpcSrvc, redisStrg)

	api.SetUpApi(r, h, cfg)

	fmt.Println("Start api gateway...")

	err = r.Run(cfg.HTTPPort)
	if err != nil {
		return
	}
}
