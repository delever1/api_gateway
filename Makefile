CURRENT_DIR=$(shell pwd)

proto-gen:
	./scripts/gen-proto.sh  ${CURRENT_DIR}

pull-proto-module:
	git submodule update --init --recursive

update-proto-module:
	git submodule update --remote --merge

swag:
	swag init -g api/api.go -o api/docs

run:
	go run cmd/main.go