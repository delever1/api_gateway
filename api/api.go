package api

import (
	"delever/api_gateway/api/handler"
	"delever/api_gateway/config"

	_ "delever/api_gateway/api/docs"

	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	"github.com/gin-gonic/gin"
)

// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func SetUpApi(r *gin.Engine, h *handler.Handler, cfg config.Config) {
	r.Use(customCORSMiddleware())
	r.Use(MaxAllowed(500))

	v1 := r.Group("/v1")

	v1.POST("/auth/sign-in", h.SignIn)
	v1.POST("/forgot-password", h.ForgotPassword)
	v1.POST("/update-password", h.UpdatePassword)

	product := v1.Group("/products")
	{
		product.POST("/", h.CreateProduct)
		product.GET("/", h.GetListProduct)
		product.GET("/:id", h.GetProduct)
		product.PUT("/:id", h.UpdateProduct)
		product.DELETE("/:id", h.DeleteProduct)
	}

	category := v1.Group("/categories")
	{
		category.POST("/", h.CreateCategory)
		category.GET("/", h.GetListCategory)
		category.GET("/:id", h.GetCategory)
		category.PUT("/:id", h.UpdateCategory)
		category.DELETE("/:id", h.DeleteCategory)
	}

	user := v1.Group("/users")
	{
		user.POST("/", h.CreateUser)
	}

	branch := v1.Group("/branches")
	{
		branch.POST("/", h.CreateBranch)
		branch.GET("/", h.GetListBranch)
		branch.GET("/:id", h.GetBranch)
		branch.PUT("/:id", h.UpdateBranch)
		branch.DELETE("/:id", h.DeleteBranch)
		branch.GET("/active", h.GetListActiveBranch)
	}

	order := v1.Group("/orders")
	{
		order.GET("/", h.GetListOrder)
		order.PUT("/change-status/:id", h.ChangeOrderStatus)
	}

	url := ginSwagger.URL("swagger/doc.json") // The url pointing to API definition
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))
}

func MaxAllowed(n int) gin.HandlerFunc {
	var countReq int64
	sem := make(chan struct{}, n)
	acquire := func() {
		sem <- struct{}{}
		countReq++
	}

	release := func() {
		select {
		case <-sem:
		default:
		}
		countReq--
	}

	return func(c *gin.Context) {
		acquire()       // before request
		defer release() // after request

		c.Set("sem", sem)
		c.Set("count_request", countReq)

		c.Next()
	}
}

func customCORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {

		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Credentials", "true")
		c.Header("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, PATCH, DELETE, HEAD")
		c.Header("Access-Control-Allow-Headers", "Platform-Id, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Header("Access-Control-Max-Age", "3600")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}
