package handler

import (
	"delever/api_gateway/genproto/user_service"
	"delever/api_gateway/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

func (h *Handler) CreateUser(ctx *gin.Context) {
	var user = user_service.UserCreateReq{}

	err := ctx.ShouldBindJSON(&user)
	if err != nil {
		h.handlerResponse(ctx, "CreateUser", http.StatusBadRequest, err.Error())
		return
	}

	hashPass, err := helper.GeneratePasswordHash(user.Password)
	if err != nil {
		h.handlerResponse(ctx, "GeneratePasswordHash", http.StatusInternalServerError, err.Error())
		return
	}

	resp, err := h.services.UserService().Create(ctx, &user_service.UserCreateReq{
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Email:     user.Email,
		BranchId:  user.BranchId,
		Phone:     user.Phone,
		Login:     user.Login,
		Password:  string(hashPass),
	})

	if err != nil {
		h.handlerResponse(ctx, "UserService().Create", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "create user response", http.StatusOK, resp)
}
