package handler

import (
	"delever/api_gateway/config"
	"delever/api_gateway/genproto/user_service"
	"delever/api_gateway/pkg/helper"
	"delever/api_gateway/pkg/logger"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

type SignInReq struct {
	Username string `json:"login"`
	Password string `json:"password"`
	Role     string `json:"role"`
}

type SignInResp struct {
	Token string `json:"token"`
}

// 2. Auth: login(activeligini tekshirish kerak),changePassvord -> SMTP protocol

// @Router       /v1/auth/sign-in [post]
// @Summary      sign in staff
// @Description  api for sign in staff
// @Tags         auth
// @Accept       json
// @Produce      json
// @Param        staff    body     SignInReq  true  "data of staff"
// @Success      200  {object}  SignInResp
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) SignIn(ctx *gin.Context) {
	var req SignInReq
	if err := ctx.ShouldBindJSON(&req); err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		h.handlerResponse(ctx, "ShouldBindJSON()", http.StatusBadRequest, err.Error())
		return
	}

	if req.Role == "user" {
		user, err := h.services.UserService().GetByUsername(ctx, &user_service.UserGetByUsernameReq{
			Username: req.Username,
		})
		if err != nil {
			h.log.Error("error while getting user by username:", logger.Error(err))
			h.handlerResponse(ctx, "UserService().GetByUsername", http.StatusBadRequest, err.Error())
			return
		}

		if err = helper.ComparePasswords([]byte(user.Password), []byte(req.Password)); err != nil {
			h.log.Error("username or password incorrect")
			h.handlerResponse(ctx, "username or password incorrect", http.StatusBadRequest, "username or password incorrect")
			return
		}

		m := make(map[string]interface{})
		m["user_id"] = user.Id
		m["branch_id"] = user.BranchId
		m["username"] = user.Login
		m["role"] = "user"

		token, err := helper.GenerateJWT(m, config.TokenExpireTime, config.JWTSecretKey)
		if err != nil {
			h.log.Error("error while generate jwt token", logger.Error(err))
			h.handlerResponse(ctx, "error while generate jwt token", http.StatusBadRequest, err.Error())
			return
		}

		ctx.JSON(http.StatusCreated, SignInResp{Token: token})
	} else {
		courier, err := h.services.CourierService().GetByUsername(ctx, &user_service.CourierGetByUsernameReq{
			Username: req.Username,
		})
		if err != nil {
			h.log.Error("error while getting courier by username:", logger.Error(err))
			h.handlerResponse(ctx, "CourierService().GetByUsername", http.StatusBadRequest, err.Error())
			return
		}

		if err = helper.ComparePasswords([]byte(courier.Password), []byte(req.Password)); err != nil {
			h.log.Error("username or password incorrect")
			h.handlerResponse(ctx, "username or password incorrect", http.StatusBadRequest, err.Error())
			return
		}

		m := make(map[string]interface{})
		m["courier_id"] = courier.Id
		m["branch_id"] = courier.BranchId
		m["username"] = courier.Login
		m["courier"] = "courier"

		token, err := helper.GenerateJWT(m, config.TokenExpireTime, config.JWTSecretKey)
		if err != nil {
			h.log.Error("error while generate jwt token", logger.Error(err))
			h.handlerResponse(ctx, "error while generate jwt token", http.StatusBadRequest, err.Error())
			return
		}

		ctx.JSON(http.StatusCreated, SignInResp{Token: token})
	}
}

type ChangePasswordReq struct {
	Email string `json:"email"`
	Role  string `json:"role"`
}

func (h *Handler) ForgotPassword(ctx *gin.Context) {
	var req ChangePasswordReq
	if err := ctx.ShouldBindJSON(&req); err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		h.handlerResponse(ctx, "ShouldBindJSON()", http.StatusBadRequest, err.Error())
		return
	}

	email := "hasanovdiyorbek12@gmail.com"

	if req.Role == "user" {
		_, err := h.services.UserService().GetByEmail(ctx.Request.Context(), &user_service.UserGetByEmailReq{
			Email: req.Email,
		})

		if err != nil {
			h.handlerResponse(ctx, "UserService().GetByEmail", http.StatusNotFound, "user not found by this email")
			return
		}

		res, err := helper.SendMail(email, req.Email)
		if err != nil {
			h.log.Error("error while send email", logger.Error(err))
			h.handlerResponse(ctx, "SendMail()", http.StatusBadRequest, err.Error())
			return
		}

		if err = h.redisStrg.Cache().Create(ctx.Request.Context(), req.Email, res, time.Minute*3); err != nil {
			h.log.Error("error while create in redis", logger.Error(err))
			h.handlerResponse(ctx, "Cache().Create", http.StatusInternalServerError, err.Error())
			return
		}

		ctx.JSON(http.StatusOK, "message sent to email")
	} else if req.Role == "courier" {
		_, err := h.services.CourierService().GetByEmail(ctx.Request.Context(), &user_service.CourierGetByEmailReq{
			Email: req.Email,
		})

		if err != nil {
			h.handlerResponse(ctx, "CourierService().GetByEmail", http.StatusNotFound, "courier not found by this email")
			return
		}

		res, err := helper.SendMail(email, req.Email)
		if err != nil {
			h.log.Error("error while send email", logger.Error(err))
			h.handlerResponse(ctx, "SendMail()", http.StatusBadRequest, err.Error())
			return
		}

		h.redisStrg.Cache().Create(ctx.Request.Context(), req.Email, res, time.Minute*3)

		ctx.JSON(http.StatusOK, "message sent to email")
	}
}

type UpdatePasswordReq struct {
	Email       string `json:"email"`
	Code        string `json:"code"`
	NewPassword string `json:"new_password"`
	CPassword   string `json:"confirm_password"`
	Role        string `json:"role"`
}

func (h *Handler) UpdatePassword(ctx *gin.Context) {
	var req UpdatePasswordReq
	if err := ctx.ShouldBindJSON(&req); err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		h.handlerResponse(ctx, "ShouldBindJSON()", http.StatusBadRequest, err.Error())
		return
	}

	var code string

	found, err := h.redisStrg.Cache().Get(ctx.Request.Context(), req.Email, &code)
	if err != nil {
		h.log.Error("error while getting code from redis", logger.Error(err))
		h.handlerResponse(ctx, "Cache().Get", http.StatusInternalServerError, err.Error())
		return
	}

	if found {
		if req.Code != code {
			h.handlerResponse(ctx, "Invalid code", http.StatusUnauthorized, "Invalid confirmation code")
			return
		}
	} else {
		h.handlerResponse(ctx, "error not found", http.StatusNotFound, "try again")
		return
	}

	if req.NewPassword != req.CPassword {
		h.handlerResponse(ctx, "passwords mismatch", http.StatusBadRequest, "mismatch passwords")
		return
	}

	hashPass, err := helper.GeneratePasswordHash(req.NewPassword)
	if err != nil {
		h.handlerResponse(ctx, "GeneratePasswordHash", http.StatusInternalServerError, err.Error())
		return
	}

	if req.Role == "user" {
		_, err := h.services.UserService().UpdatePassword(ctx.Request.Context(), &user_service.UserUpdatePasswordReq{
			Email:    req.Email,
			Password: string(hashPass),
		})

		if err != nil {
			h.handlerResponse(ctx, "UserService().UpdatePassword", http.StatusBadRequest, err.Error())
			return
		}

		h.handlerResponse(ctx, "response user update response", http.StatusOK, "update user password")
		ctx.JSON(http.StatusOK, "updated successfully")
	} else if req.Role == "courier" {
		_, err := h.services.CourierService().UpdatePassword(ctx.Request.Context(), &user_service.CourierUpdatePasswordReq{
			Email:    req.Email,
			Password: string(hashPass),
		})

		if err != nil {
			h.handlerResponse(ctx, "CourierService().UpdatePassword", http.StatusBadRequest, err.Error())
			return
		}

		h.handlerResponse(ctx, "response courier update response", http.StatusOK, "update courier password")
		ctx.JSON(http.StatusOK, "updated successfully")
	}
}
