package handler

import (
	"delever/api_gateway/genproto/order_service"
	"delever/api_gateway/pkg/logger"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func (h *Handler) CreateOrder(ctx *gin.Context) {
	var order = order_service.OrderCreateReq{}

	if err := ctx.ShouldBindJSON(&order); err != nil {
		h.handlerResponse(ctx, "CreateOrder", http.StatusBadRequest, err.Error())
		return
	}

	// respClient, err := h.services.ClientService().GetById(ctx.Request.Context(), &user_service.ClientIdReq{
	// 	Id: order.ClientId,
	// })

	// var discountPrice float32

	// 	switch respClient.DiscountType {
	// 	case "fixed":
	// 		discountPrice = respClient.DiscountAmount
	// 	case "percent":
	// 		discountPrice = order.Price * respClient.DiscountAmount
	// 	}

	// 	respBranch, err := h.services.BranchService().GetById(ctx.Request.Context(), &user_service.BranchIdReq{Id: order.BranchId})
	// 	if err != nil {
	// 		h.handlerResponse(ctx, "BranchService().GetById", http.StatusBadRequest, err.Error())
	// 		return
	// 	}

	// 	respDeliveryTarif, err := h.services.DeliveryTarifService().GetById(ctx.Request.Context(), &order_service.TarifIdReq{Id: respBranch.DeliveryTarifId})
	// 	if err != nil {
	// 		h.handlerResponse(ctx, "DeliveryTarifService().GetById", http.StatusBadRequest, err.Error())
	// 		return
	// 	}

	//	resp, err := h.services.OrderService().Create(ctx, &order_service.OrderCreateReq{
	//		ClientId:      order.ClientId,
	//		BranchId:      order.BranchId,
	//		Typ:           order.Typ,
	//		Address:       order.Address,
	//		Price:         order.Price,
	//		DeliveryPrice: order.DeliveryPrice,
	//	})
}

func (h *Handler) GetListOrder(ctx *gin.Context) {
	page, err := strconv.Atoi(ctx.DefaultQuery("page", "1"))
	if err != nil {
		h.handlerResponse(ctx, "error get page", http.StatusBadRequest, err.Error())
		return
	}

	limit, err := strconv.Atoi(ctx.DefaultQuery("limit", "10"))
	if err != nil {
		h.handlerResponse(ctx, "error get limit", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.OrderService().GetList(ctx.Request.Context(), &order_service.OrderGetListReq{
		Page:        int64(page),
		Limit:       int64(limit),
		OrderId:     ctx.Query("order_id"),
		ClientId:    ctx.Query("client_id"),
		BranchId:    ctx.Query("branch_id"),
		Typ:         ctx.Query("type"),
		CourierId:   ctx.Query("courier_id"),
		PriceFrom:   ctx.Query("price_from"),
		PriceTo:     ctx.Query("price_to"),
		PaymentType: ctx.Query("payment_type"),
	})

	if err != nil {
		h.handlerResponse(ctx, "error GetListOrder", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list order response", http.StatusOK, resp)
}

type ChangeOrderStatusReq struct {
	Status string `json:"status"`
}

// ChangeStatusOrder godoc
// @Router       /v1/orders/change-status/{id} [put]
// @Summary      Update an existing order
// @Description  Update an existing order with the provided details
// @Tags         orders
// @Accept       json
// @Produce      json
// @Param        id       path    int     true    "Order ID to update"
// @Param        status   body    ChangeOrderStatusReq true    "Updated data for the order"
// @Success      200  {object}  order_service.OrderChangeStatusResp
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) ChangeOrderStatus(ctx *gin.Context) {
	var req ChangeOrderStatusReq
	err := ctx.ShouldBindJSON(&req)
	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		ctx.JSON(http.StatusBadRequest, "invalid body")
		return
	}
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		h.handlerResponse(ctx, "bad request", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.OrderService().ChangeStatus(ctx.Request.Context(), &order_service.OrderChangeStatusReq{
		Id:     int64(id),
		Status: req.Status,
	})

	if err != nil {
		h.handlerResponse(ctx, "OrderService().ChangeStatus", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "response order change status", http.StatusOK, resp.Msg)
}
